// Map:
// .map() lo que hace es TRANSFORMAR.
// Es INMUTABLE, no modifica el array original, sino que crea uno nuevo con la
// "transformación" aplicada.
// Además, mantienes el mismo `length` que el array orginal, te devuelve en el
// nuevo array la misma cantidad que el array que le aplicaste el método.

const letters = ['a', 'b', 'c'];

const newArray = letters.map(item => item + '++');
// const newArray = [];
// for (let index = 0; index < letters.length; index++) {
//     const element = letters[index];
//     newArray.push(element + '++');
// }
console.log('original', letters);
console.log('new', newArray);