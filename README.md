# Manipulación de Arrays en JavaScript

En este curso conocerás los arrays en JavaScript, desde como crearlos hasta los distintos métodos que tiene.
+ Optimiza tu trabajo con estructuras de datos
+ Aprende y aplica los principales métodos de los arrays
+ Descubre los fundamentos del 

## Contenido del Curso

### Fundamentos del manejo de arrays
+ ForEach
+ Mutable o Inmutable
+ Map
+ Map Reloaded
+ Filter
+ Reduce
+ Reduce Reloaded

### Métodos de JavaScript
+ Some
+ Every
+ Find y FindIndex
+ Includes
+ Join
+ Concat
+ Flat
+ FlatMap
+ Mutable functions
+ Sort
